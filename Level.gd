extends Control
class_name Level


signal selection_changed(selected)
signal change_hovering(level: Level, is_hovering: bool)

var selected := false

var grid_size := Vector2(1, 1)
var grid_position := Vector2.ZERO
var resizeble := false


@onready var selection : ColorRect = %Selection
@onready var resize_collision : CollisionShape2D = %ResizeCollisizion

func _ready() -> void:
#	Debug.debug('is_hovered', self, 'is_hovered', true)
	pass

func _process(delta: float) -> void:
	$Label.text = str(grid_position) + '\n' + str(position)

func switch_selected() -> void:
	selected = !selected
	selection.visible = selected
	emit_signal('selection_changed', self)


func _on_gui_input(event: InputEvent) -> void:
	if Input.is_action_just_pressed('click') \
	and (Input.is_action_pressed('modal') or !selected):
		switch_selected()


func is_hovered() -> bool:
	var mouse := get_local_mouse_position()
	return mouse.x > 0 and mouse.y > 0 and mouse.x < size.x and mouse.y < size.y


func _signal_hover_change(is_hovering: bool) -> void:
	emit_signal('change_hovering', self, is_hovering)


func _on_mouse_entered() -> void:
	_signal_hover_change(true)


func _on_mouse_exited() -> void:
	_signal_hover_change(false)


func _on_resize_area_mouse_entered() -> void:
	print('res')
	resizeble = true

func _on_resize_area_mouse_exited() -> void:
	print('unres')
	resizeble = false

func resize(new_size, new_grid_size):
	size = new_size
	grid_size = new_grid_size
