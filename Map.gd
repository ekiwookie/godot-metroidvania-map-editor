extends Panel

var level = preload("res://Level.tscn")
var multiple_selection = preload("res://MultipleSelection.tscn")
var multiple_selection_instance : MultipleSelection = null

var cell_size := Settings.CELL_SIZE
var world_size := Vector2(30, 30)

var grid := {}
var levels = []

var selecting := false
var dragging := false
var resizing := false
var dragging_start_mouse := Vector2.ZERO
var resizing_start_mouse := Vector2.ZERO
var resizing_level : Level = null
var hovered_levels := []
var selected := []


func _ready() -> void:
	custom_minimum_size = world_size * cell_size
	_generate_grid()


func _generate_grid():
	for x in world_size.x:
		for y in world_size.y:
			grid[Vector2(x, y)] = null
			var rect = ReferenceRect.new()
			rect.border_color = Color(1, 0, 0, 0.1)
			rect.position = grid_to_world(Vector2(x,y))
			rect.size = Vector2(cell_size, cell_size)
			rect.editor_only = false
			rect.z_index = 0
			rect.mouse_filter = Control.MOUSE_FILTER_IGNORE
			add_child(rect)
			var label = Label.new()
			label.position = grid_to_world(Vector2(x,y))
			label.text = str(Vector2(x,y))
			add_child(label)


# Преобразует сектор в координаты
func grid_to_world(position: Vector2) -> Vector2:
	return position * cell_size


# Преобразует координаты в сектор
func world_to_grid(position: Vector2) -> Vector2:
	return floor(position / cell_size)


func position_to_world(position: Vector2) -> Vector2:
	return world_to_grid(position) * cell_size

# TODO: переименовать в to_world
func local_mouse_to_world() -> Vector2:
	return position_to_world(get_local_mouse_position())


func _add_level() -> void:
	var level_instance = level.instantiate()
	level_instance.position = position_to_world(get_local_mouse_position())
	level_instance.grid_position = world_to_grid(level_instance.position)
#	print('added level at ', level_instance.position)
	levels.append(level_instance)
	level_instance.connect('selection_changed', _selection_changed)
	add_child(level_instance)


func _selection_changed(level) -> void:
	if level.selected:
		selected.append(level)
	else:
		selected.erase(level)
#	print('selected: ', selected)


func _get_hovered_level(selected := false) -> Level:
	for level in levels:
		if level.is_hovered():
			if selected and level.selected:
				return level
			elif not selected:
				return level
	return null


func _free_selection() -> void:
	for level in levels:
		if level.selected and not level.is_hovered():
			level.switch_selected()


func _drag() -> void:
	var distance := local_mouse_to_world() - dragging_start_mouse
	for level in selected:
#		print('drag ', dragging_start_mouse, distance, grid_to_world(level.grid_position) + distance)
		level.position = grid_to_world(level.grid_position) + distance


func _end_drag() -> void:
	dragging_start_mouse = Vector2.ZERO
	for level in selected:
		level.grid_position = world_to_grid(level.position)
	dragging = false


func _start_drag() -> void:
	dragging = true
	dragging_start_mouse = local_mouse_to_world()


func _start_selecting() -> void:
	selecting = true
	multiple_selection_instance = multiple_selection.instantiate()
	add_child(multiple_selection_instance)


func _end_selecting() -> void:
	selecting = false
	print(multiple_selection_instance.area.get_overlapping_bodies())
	multiple_selection_instance.collision.disabled = true
	multiple_selection_instance.queue_free()


func _start_resizing(hovered_level) -> void:
	resizing = true
	resizing_start_mouse = local_mouse_to_world()
	resizing_level = hovered_level
	resizing_level.resize_collision.disabled = true


func _resizing() -> void:
	var distance := local_mouse_to_world() - grid_to_world(resizing_level.grid_position)
	if distance.x < 0 or distance.y < 0:
		return
	resizing_level.size = Vector2(cell_size, cell_size) + distance


func _end_resizing() -> void:
	resizing = false
	resizing_level.grid_size = world_to_grid(resizing_level.size)
	resizing_level.resize_collision.position = Vector2(.87 * cell_size, .87 * cell_size) + resizing_level.grid_size * cell_size - Vector2(cell_size, cell_size)
	resizing_level.resize_collision.disabled = false
	resizing_level = null


func _on_gui_input(event: InputEvent) -> void:
	var hovered_level : Level = _get_hovered_level()
	if event is InputEventMouseMotion:
		if hovered_level and not dragging and not selecting and not resizing and Input.is_action_pressed('click'):
			if hovered_level.resizeble:
				_start_resizing(hovered_level)
			elif not resizing:
				_start_drag()
		elif dragging:
			_drag()
		elif resizing:
			print('sizing')
			_resizing()
		elif Input.is_action_pressed('click') and not selecting and not dragging:
			_start_selecting()
	if event is InputEventMouseButton:
		if not event.pressed and dragging:
			_end_drag()
		elif not event.pressed and selecting:
			_end_selecting()
		elif not event.pressed and resizing:
			_end_resizing()
		elif not hovered_level:
			if event.double_click:
				_add_level()
			elif not Input.is_action_pressed('modal'):
				_free_selection()
		elif not hovered_level.selected:
			if not Input.is_action_pressed('modal'):
				_free_selection()
			hovered_level.switch_selected()
		elif not event.pressed and not Input.is_action_pressed('modal'):
			_free_selection()

