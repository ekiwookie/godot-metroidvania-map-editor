extends Control
class_name MultipleSelection


var start_local := Vector2.ZERO
var start_global := Vector2.ZERO

@onready var area : Area2D = %Area
@onready var collision : CollisionPolygon2D = %Collision

func _ready() -> void:
	start_global = get_global_mouse_position()
	start_local = get_local_mouse_position()

	global_position = start_global
	size = get_local_mouse_position()

#	Debug.debug('get_global_mouse_position', self, 'get_global_mouse_position', true)
#	Debug.debug('start_global', self, 'start_global', false)

func _input(event: InputEvent) -> void:
	if event is InputEventMouseMotion:
		_change_size()


func _change_size() -> void:
	var local_mouse : Vector2 = get_local_mouse_position()
	var global_mouse : Vector2 = get_global_mouse_position()
	var global_vector : Vector2 = start_global - global_mouse

	if global_mouse.x > start_global.x and global_mouse.y > start_global.y:
#		right-down
		global_position = start_global
		size = local_mouse
	elif global_mouse.x < start_global.x and global_mouse.y < start_global.y:
#		left-up
		size = global_vector
		global_position = global_mouse
	elif global_mouse.x > start_global.x and global_mouse.y < start_global.y:
#		right-top
		global_position = Vector2(start_global.x, global_mouse.y)
		size = Vector2(-global_vector.x, +global_vector.y)
	elif global_mouse.x < start_global.x and global_mouse.y > start_global.y:
#		left-down
		global_position = Vector2(global_mouse.x, start_global.y)
		size = Vector2(global_vector.x, -global_vector.y)

	collision.scale = size


func _on_area_body_entered(body: Node2D) -> void:
	if Input.is_action_pressed('modal') or not body.get_owner().selected:
		body.get_owner().switch_selected()


func _on_area_body_exited(body: Node2D) -> void:
	if body in area.get_overlapping_bodies() or collision.disabled:
		return
	if Input.is_action_pressed('modal') or body.get_owner().selected:
		body.get_owner().switch_selected()


