extends Control

@onready var panel : Panel = $SubViewportContainer/SubViewport/Map
@onready var camera : Camera2D = %Camera2D
@onready var sub_viewport_container : SubViewportContainer = $SubViewportContainer

@onready var h_scroll_bar : HScrollBar = %HScrollBar
@onready var v_scroll_bar : VScrollBar = %VScrollBar


const ZOOM_STEP := 0.2


var panning := false


func _ready() -> void:
	_recalculate_scrolls()


func _get_limits() -> Vector2:
	return (panel.size * camera.zoom  - size) / camera.zoom


func _recalculate_scrolls() -> void:
	h_scroll_bar.max_value = _get_limits().x + size.x * camera.zoom.x
	h_scroll_bar.page = size.x * camera.zoom.x
	v_scroll_bar.max_value = _get_limits().y + size.y * camera.zoom.x
	v_scroll_bar.page = size.y * camera.zoom.y

func zoom(amount) -> void:
	var previous_zoom = camera.zoom
	camera.zoom += Vector2(amount, amount)
	panel.position -= (size/2 - get_local_mouse_position()) * (camera.zoom - previous_zoom)

	_recalculate_scrolls()
	_recalculate()


func _recalculate() -> void:
	panel.position.x = -clamp(-panel.position.x, 0, _get_limits().x)
	panel.position.y = -clamp(-panel.position.y, 0, _get_limits().y)
	h_scroll_bar.value = -panel.position.x
	v_scroll_bar.value = -panel.position.y


func zoom_in() -> void:
	if camera.zoom.x <= 5:
		zoom(ZOOM_STEP)


func zoom_out() -> void:
	if panel.size * (camera.zoom - Vector2(ZOOM_STEP, ZOOM_STEP)) > size:
		zoom(-ZOOM_STEP)


func _on_gui_input(event: InputEvent) -> void:
	if event.is_action_pressed('pan_with_mouse'):
		panning = true
	elif event.is_action_released('pan_with_mouse'):
		panning = false
	if event is InputEventMouseMotion and panning == true:
		panel.position += event.relative
		_recalculate()
	if Input.is_action_just_pressed('zoom_in'):
		zoom_in()
	elif Input.is_action_just_pressed('zoom_out'):
		zoom_out()
	elif Input.is_action_pressed('pan_with_mouse'):
		pass


func _on_h_scroll_bar_scrolling() -> void:
	panel.position.x = -h_scroll_bar.value


func _on_v_scroll_bar_scrolling() -> void:
	panel.position.y = -v_scroll_bar.value

